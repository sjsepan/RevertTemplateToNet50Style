# Revert C# Project Template To .Net 5.0 Style

## About

The following is based upon a forum comment I posted a while back. The example is specific to .Net 6.0. You can do the same with later versions, and I used it successfully with .Net 8.0.

The `dotnet new` project templates in .Net 6  and later may default to some of the following values:

 Setting | Value |
|---|---|
| UseProgramMain | false |
| ImplicitUsings | true |
| Nullable | true |
| FileScopedNamespaces | true |

If you don't want to use these features, the advice often given is to use (e.g. in a console) `dotnet new console --framework net5.0`, and then edit the .net version in the project file to 6.
I am not aware of a utility to perform these edits automatically (at least not on Linux). What I managed to do is edit the template package manually.

## Platform

This task was performed on Linux, where I cannot achieve the desired effect using a checkbox setting in Visual Studio. This will facilitate changing the templates for VSCode / VSCodium.

## Edits

Here we are going to modify the console project template. Substitute the version number that you have for the version in the example.
Locate the template package 'microsoft.dotnet.common.projecttemplates.6.0.6.0.100.nupkg' in '/usr/share/dotnet/templates/6.0.1' (or '~/.dotnet/templates/6.0.1'). Make a copy[^1], then (for the example of a Console app) extract and edit the contents of 'template.json' at 'content/ConsoleApplication-CSharp/.template.config'. Find the features mentioned, and for each that you want to alter, change their value to the opposite:

 Setting | Value |
|---|---|
| UseProgramMain | true |
| ImplicitUsings | false |
| Nullable | false |
| FileScopedNamespaces | false |

Save the file and then copy the 'contents' folder back into the .nupkg archive. Then copy the modified .nupkg file back to the location where you found it.

The next time you do `dotnet new console`, you should get the .Net 6 project generated with the old style.

## Contact

Stephen J Sepan
<sjsepan@yahoo.com>
4-26-2024

--------------

[^1]: Note: if .Net is installed under the user's home directory, you can edit and save the file directly, without making copies and using `sudo` to write the package file back.
